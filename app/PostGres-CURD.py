import pandas as pd 
import psycopg2
import psycopg2.extras as extras
from flask import Flask, jsonify, request
from flask import request
app = Flask(__name__)
 
@app.route('/')
 
def CRUD_OP_function():
 
    class PostGres_CURD:
 
        def Read_Data(self,FileName):
            ReadDF=pd.read_csv(FileName)
            return ReadDF
 
        def Database_Connection(self,database,user,password,host,port):
            connection = psycopg2.connect(database="postgres", user = "postgres", password = "admin", host = "postgresql", port = "5432")
            return connection
 
        def CreateTable(self,connection,TableName,df):
            # replace the extra data which we cannot pass in schema of table 
            schema=str(df.dtypes)
            schema = schema.replace("dtype: object", " ") 
            schema= schema.replace("int64", "int,")
            schema= schema.replace("float64", "float,")
            schema= schema.replace("object", "text,")
            schema=schema[:-3].strip()
            cursor = connection.cursor()
            try:
                cursor.execute('CREATE TABLE '+TableName+' ('+schema+');')
            except (Exception, psycopg2.DatabaseError) as error:
                connection.rollback()
                cursor.close()
                return 1
            connection.commit()
        
        #Insert Records
        def InsertReocrds(self,connection,TableName,df):
            tuples = [tuple(x) for x in df.to_numpy()]
            cols = ','.join(list(df.columns))
            # SQL quert to execute
            query = "INSERT INTO %s(%s) VALUES %%s" % (TableName, cols)
            cursor = connection.cursor()
            try:
                extras.execute_values(cursor, query, tuples)
                connection.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                connection.rollback()
                cursor.close()
                return 1
 
    ####### DEFINE PARAMETER #########
    FIleName_Location ='Demo.csv'
    #getTableName = request.args['TableName']
    # TableName ='"pythonDemo".EmployeeDetail_3'
    TableName = 'EmployeeDetail_5'
    #Select Transaction Type ####  INSERT -- CREATE  -- UPDATE  --DELETE --RETRIEVE 
    #getTransaction = request.args['Transaction']
    
    Transaction = 'CREATE' 
    ######## DataBase Credintials ###########
    database="postgres"
    user = "postgres" 
    password = "admin"
    host = "postgresql"
    port = "5432"
    ##### CREATE OBJECT #############
    DB_Object=PostGres_CURD()
    connection=DB_Object.Database_Connection(database,user,password,host,port)
    df=DB_Object.Read_Data(FIleName_Location)
    
    if Transaction == 'CREATE':
        DB_Object.CreateTable(connection,TableName,df)
 
    elif Transaction == 'INSERT':
        DB_Object.InsertReocrds(connection,TableName,df)
    
            
    return "Command Successfully Performed"
 
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')