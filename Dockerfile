FROM tiangolo/uwsgi-nginx-flask
COPY requirements.txt .
RUN pip install -r requirements.txt
EXPOSE 5000
COPY app /app
WORKDIR /app
CMD ["python", "PostGres-CURD.py"]